from c_parser import *

prologue = '''
include ./masm32/include/masm32rt.inc

includelib ./masm32/lib/kernel32.lib
includelib ./masm32/lib/masm32.lib
includelib ./masm32/lib/user32.1ib

main PROTO

.data

.code

start:
	invoke main

    fn MessageBox,0,str$(eax), "3-21-Python-IO-81-Savotikov", MB_OK

    invoke ExitProcess, 0

'''

class AsmGen:
	def __init__(self, program):
		self.prog = program
		self.var_map = {}
		self.stack_index = -4
		self.stack_idx_old = 0
		self.var_scope = -1
		self.if_label_counter = 0
		self.cycle_label_counter = 0
		self.current_cl_num = 0
		self.current_func = ''
	
	def generate(self, filename):
		buf = prologue

		for func in self.prog.funcs:
			if func.blockitems is None:
				continue
			self.current_func = func.name
			self.pushScope()
			param_offset = 8
			for param in func.parametrs:
				self.var_map[self.current_func][self.var_scope][param.name] = param_offset
				param_offset += 4
			buf += f'\n{func.name} PROC\n'
			buf += '\n\tpush ebp\n\tmov ebp, esp\n'
			for i in func.blockitems:
				buf += self.process_block_item(i)
			self.popScope()
			buf += f'\n{func.name} ENDP\n'
		buf += 'END start'
		with open(filename, 'w') as asm:
			asm.write(buf)

	def tb(self):
		return '\t' * (self.var_scope + 1)

	def findVarOffset(self, var_name):
		for i in range(self.var_scope, -1, -1):
			if var_name in self.var_map[self.current_func][i].keys():
				return self.var_map[self.current_func][i][var_name]
		
	def pushScope(self):
		self.stack_idx_old = self.stack_index
		self.var_scope += 1
		if self.current_func in self.var_map.keys():
			self.var_map[self.current_func].append({})
		else:
			self.var_map[self.current_func] = [{}]

	def popScope(self):
		self.var_scope -= 1
		self.var_map[self.current_func].pop()
		esp_change = self.stack_idx_old - self.stack_index
		self.stack_index = self.stack_idx_old
		if esp_change > 0:
			return f'{self.tb()}add esp, {esp_change}\n'
		return ''

	def process_unary_expression(self, expr):
		buf = self.process_expression(expr.expr)
		if expr.expr_type == ExprType.EXPR_UN_LNEG:
			buf += self.tb() + 'cmp eax, 0\n' + self.tb() + 'xor eax, eax\n' + self.tb() + 'sete al\n'
		elif expr.expr_type == ExprType.EXPR_UN_NEG:
			buf += f'{self.tb()}neg eax\n'
		elif expr.expr_type == ExprType.EXPR_UN_BCOMP:
			buf += f'{self.tb()}not eax\n'
		elif expr.expr_type == ExprType.EXPR_UN_PRDEC:
			buf +=  f'{self.tb()}dec eax\n{self.tb()}mov [ebp+{self.findVarOffset(expr.expr.var_name)}], eax\n'
		return buf

	def process_binary_expression(self, expr):
		buf = ''
		if expr.expr_type == ExprType.EXPR_BIN_SUB:
			buf = self.process_expression(expr.expr_right)
			buf +=  f'{self.tb()}push eax\n'
			buf += self.process_expression(expr.expr_left)
			buf += f'{self.tb()}pop ecx\n{self.tb()}sub eax, ecx\n'
		elif expr.expr_type == ExprType.EXPR_BIN_ADD:
			buf = self.process_expression(expr.expr_left)
			buf += f'{self.tb()}push eax\n'
			buf += self.process_expression(expr.expr_right)
			buf += f'{self.tb()}pop ecx\n{self.tb()}add eax, ecx\n'
		elif expr.expr_type == ExprType.EXPR_BIN_LEQ:
			buf = self.process_expression(expr.expr_right)
			buf += f'{self.tb()}push eax\n'
			buf += self.process_expression(expr.expr_left)
			buf += f'{self.tb()}pop ecx\n{self.tb()}cmp eax, ecx\n{self.tb()}setle al\n'
		elif expr.expr_type == ExprType.EXPR_ASSIGN:
			buf = self.process_expression(expr.expr)
			buf += f'{self.tb()}mov [ebp+{self.findVarOffset(expr.var_expr.var_name)}], eax\n'
		return buf

	def process_expression(self, expr):
		if expr.node_type == NodeType.NODE_UN_EXPR:
			return self.process_unary_expression(expr)
		elif expr.node_type == NodeType.NODE_BIN_EXPR:
			return self.process_binary_expression(expr)
		elif expr.node_type == NodeType.NODE_CONST_EXPR:
			return f'{self.tb()}mov eax, {expr.value}\n'
		elif expr.node_type == NodeType.NODE_VAR_EXPR:
			return f'{self.tb()}mov eax, [ebp+{self.findVarOffset(expr.var_name)}]\n'
		elif expr.node_type == NodeType.NODE_CALL_EXPR:
			buf = ''
			esp_change = 0
			expr.arguments.reverse()
			for arg in expr.arguments:
				buf += self.process_expression(arg)
				buf += f'{self.tb()}push eax\n'
				esp_change += 4
			buf += f'{self.tb()}call {expr.funcname}\n'
			if esp_change > 0:
				buf += f'{self.tb()}add esp, {esp_change}\n'
			return buf

	def process_statement(self, stmt):
		if stmt.stmt_type == StmtType.STMT_CONST:
			return self.process_expression(stmt.expr)
		elif stmt.stmt_type == StmtType.STMT_BREAK:
			return f'{self.tb()}jmp _endcycle{self.current_cl_num}\n'
		elif stmt.stmt_type == StmtType.STMT_CONT:
			return f'{self.tb()}jmp _continue{self.current_cl_num}\n'
		elif stmt.stmt_type == StmtType.STMT_RETURN:
			b = self.process_expression(stmt.expr)
			return b + f'{self.tb()}mov esp, ebp\n{self.tb()}pop ebp\n{self.tb()}ret\n'
		elif stmt.stmt_type == StmtType.STMT_COMP:
			self.pushScope()
			buf = ''
			for i in stmt.blockitems:
				buf += self.process_block_item(i)
			buf += self.popScope()
			return buf
		elif stmt.stmt_type == StmtType.STMT_COND:
			self.if_label_counter += 1
			lb_cnt = self.if_label_counter
			buf = self.process_expression(stmt.expr)
			buf += f'{self.tb()}cmp eax, 0\n{self.tb()}je _false{lb_cnt}\n'
			self.pushScope()
			if stmt.blockitem_t is not None:
				for i in stmt.blockitem_t:
					buf += self.process_block_item(i)
			buf += self.popScope()
			buf += f'{self.tb()}jmp _endif{lb_cnt}\n{self.tb()}_false{lb_cnt}:\n'
			self.pushScope()
			if stmt.blockitem_f is not None:
				for i in stmt.blockitem_f:
					buf += self.process_block_item(i)
			buf += self.popScope()
			buf += f'{self.tb()}_endif{lb_cnt}:\n'
			return buf
		elif stmt.stmt_type == StmtType.STMT_WHILE:
			self.cycle_label_counter += 1
			cl_num = self.current_cl_num
			lb_cnt = self.cycle_label_counter
			self.current_cl_num = lb_cnt
			buf = f'{self.tb()}_cycle{lb_cnt}:\n'
			buf += self.process_expression(stmt.expr)
			buf += f'{self.tb()}cmp eax, 0\n{self.tb()}je _endcycle{lb_cnt}\n'
			buf += self.process_statement(stmt.statement)
			buf += f'{self.tb()}_continue{lb_cnt}:\n'
			buf += f'{self.tb()}jmp _cycle{lb_cnt}\n'
			buf += f'{self.tb()}_endcycle{lb_cnt}:\n'
			self.current_cl_num = cl_num
			return buf
		elif stmt.stmt_type == StmtType.STMT_FOR:
			self.cycle_label_counter += 1
			cl_num = self.current_cl_num
			lb_cnt = self.cycle_label_counter
			self.current_cl_num = lb_cnt
			buf = self.process_block_item(stmt.blockitem_decl)
			buf += f'{self.tb()}_cycle{lb_cnt}:\n'
			buf += self.process_expression(stmt.expr1)
			buf += f'{self.tb()}cmp eax, 0\n{self.tb()}je _endcycle{lb_cnt}\n'
			buf += self.process_statement(stmt.statement)
			buf += f'{self.tb()}_continue{lb_cnt}:\n'
			buf += self.process_expression(stmt.expr2)
			buf += f'{self.tb()}jmp _cycle{lb_cnt}\n'
			buf += f'{self.tb()}_endcycle{lb_cnt}:\n'
			self.current_cl_num = cl_num
			return buf



	def process_declaration(self, stmt):
		buf = ''
		if stmt.expr != None:
			buf += self.process_expression(stmt.expr)
		buf += f'{self.tb()}push eax\n'
		self.var_map[self.current_func][self.var_scope][stmt.name] = self.stack_index
		self.stack_index -= 4
		return buf

	def process_block_item(self, block_item):
		if block_item.block_type == BlockType.BLOCK_STMT:
			return self.process_statement(block_item.item)
		elif block_item.block_type == BlockType.BLOCK_DECL:
			return self.process_declaration(block_item.item)
	