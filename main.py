from c_lexer import Lexer
from c_parser import Parser
from c_parser import Program
from c_parser import ParserSyntaxError
from asmgen import AsmGen

def main():
	inp = ''
	with open('main.c', 'r') as src:
		inp = src.read()
	l = Lexer(['int', 'float', 'return', 'if', 'else', 'while', 'break', 'continue', 'for'])
	tokens = l.lex(inp)

	for t in tokens:
		print(t)
	
	parser = Parser()
	try:
		prog = parser.parse(tokens)
		print(prog)
		gen = AsmGen(prog)
		gen.generate('main.asm')
	except ParserSyntaxError as e:
		print(e.what())

if __name__ == "__main__":
	main()