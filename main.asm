
include ./masm32/include/masm32rt.inc

includelib ./masm32/lib/kernel32.lib
includelib ./masm32/lib/masm32.lib
includelib ./masm32/lib/user32.1ib

main PROTO

.data

.code

start:
	invoke main

    fn MessageBox,0,str$(eax), "3-21-Python-IO-81-Savotikov", MB_OK

    invoke ExitProcess, 0


foo PROC

	push ebp
	mov ebp, esp
	mov eax, 8
	mov esp, ebp
	pop ebp
	ret

foo ENDP

main PROC

	push ebp
	mov ebp, esp
	call foo
	push eax
	mov eax, [ebp+-4]
	push eax
	call bar
	add esp, 4
	push eax
	mov eax, 1
	_cycle1:
	mov eax, 5
	push eax
	mov eax, [ebp+-8]
	pop ecx
	cmp eax, ecx
	setle al
	cmp eax, 0
	je _endcycle1
		call foo
		mov [ebp+-4], eax
		_cycle2:
		mov eax, 10
		push eax
		mov eax, [ebp+-4]
		pop ecx
		cmp eax, ecx
		setle al
		cmp eax, 0
		je _endcycle2
			mov eax, [ebp+-4]
			push eax
			mov eax, [ebp+-8]
			pop ecx
			add eax, ecx
			push eax
			call bar
			add esp, 4
		_continue2:
		mov eax, [ebp+-4]
		push eax
		mov eax, 1
		pop ecx
		add eax, ecx
		mov [ebp+-4], eax
		jmp _cycle2
		_endcycle2:
	_continue1:
	mov eax, [ebp+-8]
	push eax
	mov eax, 1
	pop ecx
	add eax, ecx
	mov [ebp+-8], eax
	jmp _cycle1
	_endcycle1:
	mov eax, [ebp+-4]
	not eax
	mov esp, ebp
	pop ebp
	ret

main ENDP

bar PROC

	push ebp
	mov ebp, esp
	mov eax, [ebp+8]
	push eax
	mov eax, 10
	pop ecx
	sub eax, ecx
	mov esp, ebp
	pop ebp
	ret

bar ENDP
END start