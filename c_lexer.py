import enum

class TokenType(enum.Enum):
	KEYWORD = 'KEYWORD'
	WORD = 'WORD'
	LPARENT = '('
	RPARENT = ')'
	LBRACE = '{'
	RBRACE = '}'
	OP_NEG = '-'
	OP_BCOMP = '~'
	OP_LNEG = '!'
	OP_ADD = '+'
	OP_MUL = '*'
	OP_DIV = '/'
	OP_ASSIGN = '='
	OP_LESS = '<'
	OP_LEQUAL = '<='
	OP_PRDEC = '--'
	INT_CONSTANT = 'INT_CONSTANT'
	FLOAT_CONSTANT = 'FLOAT_CONSTANT'
	CHAR_CONSTANT = 'CHAR_CONSTANT'
	QUOTE = '\''
	SEMICOLON = ';'
	COMMA = ','

	def __str__(self):
		return f'{self.value}'

class Token:
	def __init__(self, tok_type, line, pos):
		self.tok_type = tok_type
		self.line = line
		self.pos = pos

class Keyword(Token):
	def __init__(self, keyword, line, pos):
		super().__init__(TokenType.KEYWORD, line, pos)
		self.lexem = keyword

	def __str__(self):
		return f'{self.lexem} - {self.tok_type}'

class Word(Token):
	def __init__(self, word, line, pos):
		super().__init__(TokenType.WORD, line, pos)
		self.lexem = word 
	
	def __str__(self):
		return f'{self.lexem} - {self.tok_type}'

class Constant(Token):
	def __init__(self, const_type, val, line, pos):
		super().__init__(const_type, line, pos)
		self.value = val
	
	def __str__(self):
		return f'{self.value} - {self.tok_type}'

class Delimeter(Token):
	def __init__(self, delim_type, line, pos):
		super().__init__(delim_type, line, pos)
	
	def __str__(self):
		return f'{self.tok_type} - DELIMETER'

class Operator(Token):
	def __init__(self, op_type, line, pos):
		super().__init__(op_type, line, pos)
	
	def __str__(self):
		return f'{self.tok_type} - OPERATOR'

class Lexer:
	def __init__(self, keywords):
		self.line = 1
		self.pos = 1
		self.index = 0
		self.keywords = keywords
		self.delimeters = {
			'(' : TokenType.LPARENT,
			')' : TokenType.RPARENT,
			'{' : TokenType.LBRACE,
			'}' : TokenType.RBRACE,
			';' : TokenType.SEMICOLON,
			',' : TokenType.COMMA
		}
		self.operators = {
			'-' : TokenType.OP_NEG,
			'~' : TokenType.OP_BCOMP,
			'!' : TokenType.OP_LNEG,
			'+' : TokenType.OP_ADD,
			'*' : TokenType.OP_MUL,
			'/' : TokenType.OP_DIV,
			'=' : TokenType.OP_ASSIGN,
			'<' : TokenType.OP_LESS,
			'<=' : TokenType.OP_LEQUAL,
			'--' : TokenType.OP_PRDEC
		}
	
	def lex(self, inp):
		token_list = []
		while self.index < inp.__len__():
			m = inp[self.index]

			if m.isalpha():
				token_list.append(self.__lexing_word(m, inp))
			elif m.isdigit():
				token_list.append(self.__lexing_const(m, inp))
			elif m == '\n':
				self.line += 1
				self.pos = 1
				self.index += 1
			elif m in self.delimeters.keys():
				token_list.append(Delimeter(self.delimeters[m], self.line, self.pos))
				self.index += 1
				self.pos += 1
			elif m in self.operators.keys():
				if inp[self.index + 1] == '=' or (m == '-' and inp[self.index + 1] == '-' and inp[self.index + 2].isalpha()):
					self.index += 1
					self.pos += 1
					m += inp[self.index]
				token_list.append(Operator(self.operators[m], self.line, self.pos))
				self.index += 1
				self.pos += 1
			else:
				self.index += 1
				self.pos += 1
		return token_list

	def __lexing_word(self, m, inp):
		lexem = ''
		while (m.isalnum() or m == '_') and self.index < inp.__len__():
			lexem += m
			self.index += 1
			self.pos += 1
			m = inp[self.index]
		if lexem in self.keywords:
			return Keyword(lexem, self.line, self.pos)
		else:
			return Word(lexem, self.line, self.pos)
	
	def __lexing_const(self, m, inp):
		tok_type = TokenType.INT_CONSTANT
		val = ''
		while (m.isdigit() or m == '.' or m == 'o') and self.index < inp.__len__():
			if m == '.':
				tok_type = TokenType.FLOAT_CONSTANT
			val += m
			self.index += 1
			self.pos += 1
			m = inp[self.index]
		if tok_type == TokenType.INT_CONSTANT:
			if val.__len__() >= 3 and val[0] == '0' and val[1] == 'o':
				val = val[2::]
				t = 0
				bs = 8 ** (val.__len__() - 1)
				for c in val:
					t += int(c) * bs
					bs /= 8
				val = t
			else:
				val = int(val)
		elif tok_type == TokenType.FLOAT_CONSTANT:
			val = float(val)
		return Constant(tok_type, val, self.line, self.pos)