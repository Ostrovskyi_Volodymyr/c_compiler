import enum
from c_lexer import Token
from c_lexer import TokenType

class NodeType(enum.Enum):
	NODE_PROG = 0
	NODE_FUNC = 1
	NODE_DECL = 2
	NODE_STMT = 3
	NODE_CONST_EXPR = 4
	NODE_UN_EXPR = 5
	NODE_BIN_EXPR = 6
	NODE_A_EXPR = 7
	NODE_VAR_EXPR = 8
	NODE_BLOCK = 9
	NODE_CALL_EXPR = 10

class DeclType(enum.Enum):
	DECL_NONE = 0
	DECL_FUNC = 1

class StmtType(enum.Enum):
	STMT_NONE = 0
	STMT_RETURN = '[RET STMT]'
	STMT_CONST = '[CONST STMT]'
	STMT_VAR = '[VAR STMT]'
	STMT_COMP = '[COMPOUND STMT]'
	STMT_COND = '[CONDITIONAL STMT]'
	STMT_WHILE = '[WHILE STMT]'
	STMT_BREAK = '[BREAK STMT]'
	STMT_CONT = '[CONTINUE STMT]'
	STMT_FOR = '[FOR STMT]'

	def __str__(self):
		return f'{self.value}'

class ExprType(enum.Enum):
	EXPR_NONE = 0
	EXPR_INT = 'int'
	EXPR_FLOAT = 'float'
	EXPR_CHAR = 'char'
	EXPR_STR = 4
	EXPR_UN_NEG = '-'
	EXPR_UN_BCOMP = '~'
	EXPR_UN_LNEG = '!'
	EXPR_UN_PRDEC = '--w'
	EXPR_BIN_ADD = '+'
	EXPR_BIN_SUB = '-'
	EXPR_BIN_MUL = '*'
	EXPR_BIN_DIV = '/'
	EXPR_BIN_LESS = '<'
	EXPR_BIN_LEQ = '<='
	EXPR_ASSIGN = '='
	EXPR_ID = 13 
	EXPR_FUNC_CALL = '()'

	def __str__(self):
		return f'{self.value}'

class BlockType(enum.Enum):
	BLOCK_NONE = 0
	BLOCK_DECL = 1
	BLOCK_STMT = 2

class Node:
	def __init__(self, node_type, line, pos):
		self.node_type = node_type
		self.line = line
		self.pos = pos

class Program(Node):
	def __init__(self, funcs, line, pos):
		super().__init__(NodeType.NODE_PROG, line, pos)
		self.funcs = funcs

	def __str__(self):
		buf = ''
		for f in self.funcs:
			buf += f'Function {f}\n'
		return buf

class FunctionDeclaration(Node):
	def __init__(self, name, ret_type, parametrs, blockitems, line, pos):
		super().__init__(NodeType.NODE_DECL, line, pos)
		self.name = name
		self.ret_type = ret_type
		self.decl_type = DeclType.DECL_FUNC
		self.blockitems = blockitems
		self.parametrs = parametrs

	def __str__(self):
		if self.blockitems is None:
			return ''
		
		b = f'{self.name}:\nparametrs:\n'

		for p in self.parametrs:
			b += f'\t{p.var_type} {p.name}\n'

		for i in self.blockitems:
			b += f'{i}\n'
		return b

class BlockItem(Node):
	def __init__(self, block_type, item, line, pos):
		super().__init__(NodeType.NODE_BLOCK, line, pos)
		self.block_type = block_type
		self.item = item

	def __str__(self):
		return f'{self.item}\n'

class CompoundStatement(Node):
	def __init__(self, blockitems, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_COMP
		self.blockitems = blockitems

	def __str__(self):
		b = '{\n'
		for i in self.blockitems:
			b += f'{i}\n'
		return b + '}\n'

class ConditionalStatement(Node):
	def __init__(self, expr, blockitem_t, blockitem_f, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_COND
		self.expr = expr
		self.blockitem_t = blockitem_t
		self.blockitem_f = blockitem_f
	
	def __str__(self):
		b = f'if ({self.expr})' + '{\n'
		for i in self.blockitem_t:
			b += f'{i}'
		if self.blockitem_f is not None:
			b += '} else {\n'
			for i in self.blockitem_f:
				b += f'{i}'
		return b + '}\n'

class VariableDeclaration(Node):
	def __init__(self, var_type, name, init_expr, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_VAR
		self.var_type = var_type
		self.name = name
		self.expr = init_expr

	def __str__(self):
		b = f'{self.var_type} {self.name}[VAR DECL]'
		if self.expr != None:
			b += f' = {self.expr}'
		b += ';\n'
		return b

class ConstStatement(Node):
	def __init__(self, expr, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.expr = expr
		self.stmt_type = StmtType.STMT_CONST
	
	def __str__(self):
		return f'{self.expr};\n'

class BreakStatement(Node):
	def __init__(self, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_BREAK
	
	def __str__(self):
		return 'break;\n'

class ContinueStatement(Node):
	def __init__(self, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_CONT
	
	def __str__(self):
		return 'continue;\n'

class ReturnStatement(Node):
	def __init__(self, expr, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_RETURN
		self.expr = expr

	def __str__(self):
		return f'return {self.expr};\n'

class WhileStatement(Node):
	def __init__(self, expr, stmt, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_WHILE
		self.expr = expr
		self.statement = stmt

	def __str__(self):
		return f'while ({self.expr}) {self.statement}'

class ForStatement(Node):
	def __init__(self, blockitem_decl, exp_opt1, exp_opt2, statement, line, pos):
		super().__init__(NodeType.NODE_STMT, line, pos)
		self.stmt_type = StmtType.STMT_FOR
		self.blockitem_decl = blockitem_decl
		self.expr1 = exp_opt1
		self.expr2 = exp_opt2
		self.statement = statement

	def __str__(self):
		return f'for ({self.blockitem_decl} {self.expr1}; {self.expr2}) {self.statement}'

class ConstExpression(Node):
	def __init__(self, expr_type, value, line, pos):
		super().__init__(NodeType.NODE_CONST_EXPR, line, pos)
		self.expr_type = expr_type
		self.value = value
		self.value_type = expr_type

	def __str__(self):
		return f'{self.value}[CONST]'

class UnaryExpression(Node):
	def __init__(self, expr_type, expr, line, pos):
		super().__init__(NodeType.NODE_UN_EXPR, line, pos)
		self.expr_type = expr_type
		self.expr = expr
		self.value_type = expr.value_type
	
	def __str__(self):
		return f'{self.expr_type}[UN]{self.expr}'

class BinaryExpression(Node):
	def __init__(self, expr_type, exprl, exprr, line, pos):
		super().__init__(NodeType.NODE_BIN_EXPR, line, pos)
		self.expr_type = expr_type
		self.expr_left = exprl
		self.expr_right = exprr
		self.value_type = exprl.value_type

	def __str__(self):
		return f'{self.expr_left} {self.expr_type}[BIN] {self.expr_right}'

class AssignExpression(Node):
	def __init__(self, var_expr, expr, line, pos):
		super().__init__(NodeType.NODE_BIN_EXPR, line, pos)
		self.expr_type = ExprType.EXPR_ASSIGN
		self.expr = expr
		self.var_expr = var_expr
		self.value_type = expr.value_type

	def __str__(self):
		return f'{self.var_expr} = {self.expr}'

class FuncCallExpression(Node):
	def __init__(self, funcname, value_type, arguments, line, pos):
		super().__init__(NodeType.NODE_CALL_EXPR, line, pos)
		self.expr_type = ExprType.EXPR_FUNC_CALL
		self.funcname = funcname
		self.arguments = arguments
		self.value_type = value_type

	def __str__(self):
		buf = f'{self.funcname}('

		for a in self.arguments:
			buf += f'{a},'
		return buf + ')'

class VarExpression(Node):
	def __init__(self, var_name, value_type, line, pos):
		super().__init__(NodeType.NODE_VAR_EXPR, line, pos)
		self.expr_type = ExprType.EXPR_ID
		self.value_type = value_type
		self.var_name = var_name

	def __str__(self):
		return f'{self.var_name}[VAR]'

class ParserSyntaxError(BaseException):
	def __init__(self, line, pos, err):
		self.line = line
		self.pos = pos
		self.err = err

	def what(self):
		return f'Syntax error on line {self.line}, position {self.pos}: {self.err}\n'

class FuncTableEntry:
	def __init__(self):
		self.parametrs = []
		self.ret_type = None
		self.name = None
		self.defined = False

class Parser:
	def __init__(self):
		self.index = -1
		self.func_table = {}
		self.var_table_list = {}
		self.var_scope = -1
		self.current_function = ''

	def next(self, tokens):
		if self.index < tokens.__len__() - 1:
			self.index += 1
		return tokens[self.index]

	def pushScope(self):
		self.var_scope += 1
		if self.current_function in self.var_table_list.keys():
			self.var_table_list[self.current_function].append({})
		else:
			self.var_table_list[self.current_function] = [{}]
	
	def popScope(self):
		self.var_scope -= 1
		self.var_table_list[self.current_function].pop()

	def declareVarInCurrentScope(self, var_name, stmt):
		if var_name in self.var_table_list[self.current_function][self.var_scope].keys():
			return False
		self.var_table_list[self.current_function][self.var_scope][var_name] = stmt
		return True

	def findVar(self, var_name):
		print(self.var_scope)
		if self.var_table_list[self.current_function].__len__() > 0:
			for i in range(self.var_scope, 0, -1):
				if var_name in self.var_table_list[self.current_function][i].keys():
					return self.var_table_list[self.current_function][i][var_name]
		for p in self.func_table[self.current_function].parametrs:
			if var_name == p.name:
				return p
		return None

	def parse(self, tokens):
		funcs = []

		while self.index < tokens.__len__() - 1:
			funcs.append(self.parseFunction(tokens))
		prog = Program(funcs, 0, 0)

		if self.index < tokens.__len__() - 1:
			raise ParserSyntaxError(tokens[self.index].line, tokens[self.index].pos, "Invalid token")
		return prog

	def parseFunction(self, tokens):
		var_types = {
			'int' : ExprType.EXPR_INT,
			'float' : ExprType.EXPR_FLOAT,
			'char' : ExprType.EXPR_CHAR
		}
		next_tok = tokens[self.index + 1]

		while next_tok.tok_type != TokenType.KEYWORD or next_tok.lexem not in var_types.keys():
			self.next(tokens)
			next_tok = tokens[self.index + 1]
		curr_tok = self.next(tokens)

		if curr_tok.tok_type != TokenType.KEYWORD or curr_tok.lexem not in var_types.keys():
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid return type")

		ret_type = var_types[curr_tok.lexem]
		prev_tok = curr_tok
		curr_tok = self.next(tokens)
		func_line = curr_tok.line
		func_pos = curr_tok.pos
		
		if curr_tok.tok_type != TokenType.WORD:
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid function name")

		name = curr_tok.lexem
		self.current_function = name

		prev_tok = curr_tok
		curr_tok = self.next(tokens)
		if curr_tok.tok_type != TokenType.LPARENT:
			raise ParserSyntaxError(prev_tok.line, prev_tok.pos, "Invalid syntax. Add (")

		next_tok = tokens[self.index + 1]

		parametrs = []
		while next_tok.tok_type != TokenType.RPARENT:
			p_type = self.next(tokens)
			p_name = self.next(tokens)

			if p_type.tok_type != TokenType.KEYWORD or p_type.lexem not in var_types.keys():
				raise ParserSyntaxError(p_type.line, p_type.pos, "Invalid function parametr type")
			
			if p_name.tok_type != TokenType.WORD:
				raise ParserSyntaxError(p_name.line, p_name.pos, "Invalid function parametr name")
			
			next_tok = tokens[self.index + 1]

			if next_tok.tok_type == TokenType.COMMA:
				self.next(tokens)
			parametrs.append(VariableDeclaration(var_types[p_type.lexem], p_name.lexem, None, p_name.line, p_name.pos))

		if name not in self.func_table.keys():
			self.func_table[name] = FuncTableEntry()
			self.func_table[name].parametrs = parametrs
			self.func_table[name].name = name
			self.func_table[name].ret_type = ret_type
		else:
			if self.func_table[name].parametrs.__len__() != parametrs.__len__():
				raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Does not match the number of parametrs")
			for i in range(0, parametrs.__len__()):
				if self.func_table[name].parametrs[i].var_type != parametrs[i].var_type:
					raise ParserSyntaxError(parametrs[i].line, parametrs[i].pos, "Type mismatch between parameter and argument")
		
		prev_tok = curr_tok
		curr_tok = self.next(tokens)
		if curr_tok.tok_type != TokenType.RPARENT:
			raise ParserSyntaxError(prev_tok.line, prev_tok.pos, "Invalid syntax. Add )")

		block_items = None
		next_tok = tokens[self.index + 1]
		if next_tok.tok_type == TokenType.LBRACE:
			if self.func_table[name].defined == True:
				raise ParserSyntaxError(curr_tok.line, curr_tok.pos, f'Function [{name}] is already defined')
			self.pushScope()
			block_items = self.parseBlockItem(tokens)
			self.popScope()
			curr_tok = tokens[self.index]
			if curr_tok.tok_type != TokenType.RBRACE:
				raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add }")
		elif next_tok.tok_type == TokenType.SEMICOLON:
			self.next(tokens)
		else:
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add {")
		if block_items is not None:
			block_items = block_items.item.blockitems
			self.func_table[name].defined = True
		else:
			self.func_table[name].defined = False
		func = FunctionDeclaration(name, ret_type, parametrs, block_items, func_line, func_pos)
		return func

	def parseBlockItem(self, tokens):
		var_types = {
			'int' : ExprType.EXPR_INT,
			'float' : ExprType.EXPR_FLOAT,
			'char' : ExprType.EXPR_CHAR
		}
		next_tok = tokens[self.index + 1]
		if next_tok.tok_type == TokenType.KEYWORD:
			if next_tok.lexem in var_types.keys():
				decl = self.parseDeclaration(tokens)
				curr_tok = tokens[self.index]
				return BlockItem(BlockType.BLOCK_DECL, decl, curr_tok.line, curr_tok.pos)
		stmt = self.parseStatement(tokens)
		curr_tok = tokens[self.index]
		return BlockItem(BlockType.BLOCK_STMT, stmt, curr_tok.line, curr_tok.pos)

	def parseDeclaration(self, tokens):
		var_types = {
			'int' : ExprType.EXPR_INT,
			'float' : ExprType.EXPR_FLOAT,
			'char' : ExprType.EXPR_CHAR
		}
		curr_tok = self.next(tokens)
		var_type = var_types[curr_tok.lexem]
		curr_tok = self.next(tokens)
		if curr_tok.tok_type != TokenType.WORD:
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid variable name")
		name = curr_tok.lexem
		init_expr = None
		if tokens[self.index + 1].tok_type != TokenType.SEMICOLON:
			curr_tok = self.next(tokens)
			if curr_tok.tok_type != TokenType.OP_ASSIGN:
				raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Maybe add =")
			init_expr = self.parseExpression(tokens)
		if init_expr is not None and init_expr.value_type != var_type:
			curr_tok = tokens[self.index]
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid type of initialization expression")
		curr_tok = tokens[self.index]
		stmt = VariableDeclaration(var_type, name, init_expr, curr_tok.line, curr_tok.pos)
		if not self.declareVarInCurrentScope(name, stmt):
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, f'Variable [{name}] already defined')
		prev_tok = curr_tok
		curr_tok = self.next(tokens)
		if curr_tok.tok_type != TokenType.SEMICOLON:
			raise ParserSyntaxError(prev_tok.line, prev_tok.pos, "Invalid syntax. Add ;")
		return stmt

	def _parse_blockitem_list(self, tokens):
		next_tok = tokens[self.index + 1]
		items = []
		while next_tok.tok_type != TokenType.RBRACE:
			items.append(self.parseBlockItem(tokens))
			if self.index + 1 >= tokens.__len__():
				next_tok = tokens[self.index]
				raise ParserSyntaxError(next_tok.line, next_tok.pos, "Invalid syntax. Add }")
			next_tok = tokens[self.index + 1]
		self.next(tokens)
		return items

	def parseStatement(self, tokens):
		curr_tok = self.next(tokens)
		stmt = None
		if curr_tok.tok_type == TokenType.KEYWORD:
			if curr_tok.lexem == 'return':
				expr = self.parseExpression(tokens)
				curr_tok = tokens[self.index]
				stmt = ReturnStatement(expr, curr_tok.line, curr_tok.pos)
			elif curr_tok.lexem == 'break':
				stmt = BreakStatement(curr_tok.line, curr_tok.pos)
			elif curr_tok.lexem == 'continue':
				stmt = ContinueStatement(curr_tok.line, curr_tok.pos)
			elif curr_tok.lexem == 'if':
				curr_tok = self.next(tokens)
				if curr_tok.tok_type != TokenType.LPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add (")
				expr = self.parseExpression(tokens)
				curr_tok = self.next(tokens) 
				if curr_tok.tok_type != TokenType.RPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add )")
				self.pushScope()
				next_tok = tokens[self.index + 1]
				if next_tok.tok_type != TokenType.LBRACE:
					block_true = [self.parseBlockItem(tokens)]
				else:
					self.next(tokens)
					block_true = self._parse_blockitem_list(tokens)
				self.popScope()
				block_false = None
				next_tok = tokens[self.index + 1]
				if next_tok.tok_type == TokenType.KEYWORD and next_tok.lexem == "else":
					self.next(tokens)
					self.pushScope()
					next_tok = tokens[self.index + 1]
					if next_tok.tok_type != TokenType.LBRACE:
						block_false = [self.parseBlockItem(tokens)]
					else:
						self.next(tokens)
						block_false = self._parse_blockitem_list(tokens)
					self.popScope()
				curr_tok = tokens[self.index]
				stmt = ConditionalStatement(expr, block_true, block_false, curr_tok.line, curr_tok.pos)
			elif curr_tok.lexem == 'while':
				curr_tok = self.next(tokens)
				if curr_tok.tok_type != TokenType.LPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add (")
				expr = self.parseExpression(tokens)
				curr_tok = self.next(tokens) 
				if curr_tok.tok_type != TokenType.RPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add )")
				self.pushScope()
				block_items = None
				next_tok = tokens[self.index + 1]
				if next_tok.tok_type != TokenType.LBRACE:
					block_items = [self.parseBlockItem(tokens)]
				else:
					self.next(tokens)
					block_items = self._parse_blockitem_list(tokens)
				self.popScope()
				curr_tok = tokens[self.index]
				wst = CompoundStatement(block_items, curr_tok.line, curr_tok.pos)
				stmt = WhileStatement(expr, wst, curr_tok.line, curr_tok.pos)
			elif curr_tok.lexem == 'for':
				curr_tok = self.next(tokens)
				if curr_tok.tok_type != TokenType.LPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add (")
				self.pushScope()
				blockitem_decl = None
				next_tok = tokens[self.index + 1]
				if next_tok.tok_type == TokenType.SEMICOLON:
					cexp = ConstExpression(ExprType.EXPR_INT, 1, next_tok.line, next_tok.pos)
					cst = ConstStatement(cexp, next_tok.line, next_tok.pos)
					blockitem_decl = BlockItem(BlockType.BLOCK_STMT, cst, next_tok.line, next_tok.pos)
					self.next(tokens)
				else:
					blockitem_decl = self.parseBlockItem(tokens)
				next_tok = tokens[self.index + 1]
				cond_expr = None
				if next_tok.tok_type == TokenType.SEMICOLON:
					cond_expr = ConstExpression(ExprType.EXPR_INT, 1, next_tok.line, next_tok.pos)
				else:
					cond_expr = self.parseExpression(tokens)
				curr_tok = self.next(tokens)
				if curr_tok.tok_type != TokenType.SEMICOLON:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add ;")
				next_tok = tokens[self.index + 1]
				iter_expr = None
				if next_tok.tok_type == TokenType.RPARENT:
					iter_expr = ConstExpression(ExprType.EXPR_INT, 1, next_tok.line, next_tok.pos)
				else:
					iter_expr = self.parseExpression(tokens)
				curr_tok = self.next(tokens) 
				if curr_tok.tok_type != TokenType.RPARENT:
					raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax. Add )")
				block_items = None
				next_tok = tokens[self.index + 1]
				if next_tok.tok_type != TokenType.LBRACE:
					block_items = [self.parseBlockItem(tokens)]
				else:
					self.next(tokens)
					block_items = self._parse_blockitem_list(tokens)
				self.popScope()
				curr_tok = tokens[self.index]
				fst = CompoundStatement(block_items, curr_tok.line, curr_tok.pos)
				stmt = ForStatement(blockitem_decl, cond_expr, iter_expr, fst, curr_tok.line, curr_tok.pos)
		elif curr_tok.tok_type == TokenType.LBRACE:
			self.pushScope()
			items = self._parse_blockitem_list(tokens)
			next_tok = tokens[self.index]
			stmt = CompoundStatement(items, next_tok.line, next_tok.pos)
			self.popScope()
		else:
			self.index -= 1
			expr = self.parseExpression(tokens)
			curr_tok = tokens[self.index]
			stmt = ConstStatement(expr, curr_tok.line, curr_tok.pos)
		if stmt is None:
			raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid syntax")
		if stmt.stmt_type not in [StmtType.STMT_COMP, StmtType.STMT_COND, StmtType.STMT_WHILE, StmtType.STMT_FOR]:
			prev_tok = curr_tok
			curr_tok = self.next(tokens)
			if curr_tok.tok_type != TokenType.SEMICOLON:
				raise ParserSyntaxError(prev_tok.line, prev_tok.pos, "Invalid syntax. Add ;")
		return stmt

	def parseExpression(self, tokens):
		logic = self.parseLogical(tokens)
		next_tok = tokens[self.index + 1]

		while next_tok.tok_type == TokenType.OP_ASSIGN:
			if logic.expr_type != ExprType.EXPR_ID:
				raise ParserSyntaxError(next_tok.line, next_tok.pos, "Invalid variable name in assignment")
			self.next(tokens)
			expr = self.parseExpression(tokens)
			curr_tok = tokens[self.index]
			if logic.value_type != expr.value_type:
				raise ParserSyntaxError(curr_tok.line, curr_tok.pos, "Invalid type of expression for assignment")
			return AssignExpression(logic, expr, curr_tok.line, curr_tok.pos)
		return logic
	
	def parseLogical(self, tokens):
		bin_operators = {
			TokenType.OP_LESS : ExprType.EXPR_BIN_LESS,
			TokenType.OP_LEQUAL : ExprType.EXPR_BIN_LEQ
		}

		add = self.parseAdd(tokens)
		next_tok = tokens[self.index + 1]

		while next_tok.tok_type in bin_operators.keys():
			op = bin_operators[self.next(tokens).tok_type]
			next_add = self.parseAdd(tokens)
			add = BinaryExpression(op, add, next_add, next_tok.line, next_tok.pos)
			next_tok = tokens[self.index + 1]
		return add

	def parseAdd(self, tokens):
		bin_operators = {
			TokenType.OP_ADD : ExprType.EXPR_BIN_ADD,
			TokenType.OP_NEG : ExprType.EXPR_BIN_SUB
		}

		term = self.parseTerm(tokens)
		next_tok = tokens[self.index + 1]

		while next_tok.tok_type == TokenType.OP_ADD or next_tok.tok_type == TokenType.OP_NEG:
			op = bin_operators[self.next(tokens).tok_type]
			next_term = self.parseTerm(tokens)
			term = BinaryExpression(op, term, next_term, next_tok.line, next_tok.pos)
			next_tok = tokens[self.index + 1]
		return term

	def parseTerm(self, tokens):
		bin_operators = {
			TokenType.OP_MUL : ExprType.EXPR_BIN_MUL,
			TokenType.OP_DIV : ExprType.EXPR_BIN_DIV
		}

		factor = self.parseFactor(tokens)
		next_tok = tokens[self.index + 1]

		while next_tok.tok_type == TokenType.OP_MUL or next_tok.tok_type == TokenType.OP_DIV:
			op = bin_operators[self.next(tokens).tok_type]
			next_factor = self.parseFactor(tokens)
			factor = BinaryExpression(op, factor, next_factor, next_tok.line, next_tok.pos)
			next_tok = tokens[self.index + 1]
		return factor
	
	def parseFactor(self, tokens):
		un_operators = {
			TokenType.OP_NEG : ExprType.EXPR_UN_NEG,
			TokenType.OP_BCOMP : ExprType.EXPR_UN_BCOMP,
			TokenType.OP_LNEG : ExprType.EXPR_UN_LNEG,
			TokenType.OP_PRDEC : ExprType.EXPR_UN_PRDEC
		}
		var_types = {
			TokenType.INT_CONSTANT : ExprType.EXPR_INT,
			TokenType.FLOAT_CONSTANT : ExprType.EXPR_FLOAT,
			TokenType.CHAR_CONSTANT : ExprType.EXPR_CHAR
		}
		next_tok = self.next(tokens)
		if next_tok.tok_type == TokenType.WORD and next_tok.lexem in self.func_table.keys():
			name = next_tok.lexem
			next_tok = self.next(tokens)
			if next_tok.tok_type != TokenType.LPARENT:
				raise ParserSyntaxError(next_tok.line, next_tok.pos, "Invalid syntax. Add (")
			arguments = []
			next_tok = tokens[self.index + 1]
			while next_tok.tok_type != TokenType.RPARENT:
				arguments.append(self.parseExpression(tokens))
				next_tok = self.next(tokens)
				print(next_tok)
				if next_tok.tok_type != TokenType.COMMA and next_tok.tok_type != TokenType.RPARENT:
					raise ParserSyntaxError(next_tok.line, next_tok.pos, "Invalid syntax. Add ,")
			if arguments.__len__() == 0:
				self.next(tokens)
			params = self.func_table[name].parametrs
			if arguments.__len__() != params.__len__():
				raise ParserSyntaxError(next_tok.line, next_tok.pos, "Does not match the number of parametrs")
			for i in range(0, arguments.__len__()):
				if arguments[i].value_type != params[i].var_type:
					raise ParserSyntaxError(arguments[i].line, arguments[i].pos, "Type mismatch between parameter and argument")
			return FuncCallExpression(name, self.func_table[name].ret_type, arguments, next_tok.line, next_tok.pos)
		elif next_tok.tok_type == TokenType.LPARENT:
			expr = self.parseExpression(tokens)
			if tokens[self.index + 1].tok_type != TokenType.RPARENT:
				raise ParserSyntaxError(tokens[self.index].line, tokens[self.index].pos, "Invalid syntax. Add )")
			self.next(tokens)
			return expr
		elif next_tok.tok_type in un_operators.keys():
			op = un_operators[next_tok.tok_type]
			factor = self.parseFactor(tokens)
			return UnaryExpression(op, factor, next_tok.line, next_tok.pos)
		elif next_tok.tok_type in var_types.keys():
			return ConstExpression(var_types[next_tok.tok_type], int(next_tok.value), next_tok.line, next_tok.pos)
		elif next_tok.tok_type == TokenType.WORD:
			var = self.findVar(next_tok.lexem)
			if var is None:
				raise ParserSyntaxError(next_tok.line, next_tok.pos, "Variable is not defined")
			return VarExpression(var.name, var.var_type, next_tok.line, next_tok.pos)
		else:
			raise ParserSyntaxError(next_tok.line, next_tok.pos, "Invalid syntax")
		




