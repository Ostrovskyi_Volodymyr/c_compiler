
int foo() {
	return 8;
}

int bar(int a);

int main() {
	int a = foo();
	int b = bar(a);

	for (; b <= 5; b = b + 1) {
		for (a = foo(); a <= 10; a = a + 1) {
			bar(a + b);
		}
	}
	return ~a;
}

int bar(int a) {
	return 10 - a;
}